const path = require('path');
const merge = require('webpack-merge');
const base = require('./webpack.config.base.js');


module.exports = merge(base, {
    mode: 'development',
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                loader: ['ts-loader'],
            },
            {
                test: /\.(scss|less|styl)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                    'less-loader',
                    'stylus-loader',
                ],
            },
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 9000,
        historyApiFallback: true,
    },
});
