import {Model, Collection} from 'vue-mc'

export class NoteModel extends Model {
  defaults () {
    return {
      id: null,
      title: '',
      content: '',
      done: false,
      archive: false
    }
  }

  mutations () {
    return {
      id: (id) => Number(id) || null,
      title: String,
      content: String,
      done: Boolean,
      archive: Boolean
    }
  }
}

export class NoteList extends Collection {
  model () {
    return NoteModel
  }
}

export let notes = new NoteList()
