export default {
  '/api/notes': function (request) {
    const notes = [
      {
        id: 1,
        title: 'Some note 1',
        content: 'Do anything',
        done: false
      },
      {
        id: 2,
        title: 'Some note 2',
        content: 'Do anything',
        done: false
      },
      {
        id: 3,
        title: 'Some note 3',
        content: 'Do anything',
        done: true,
        archive: true
      },
      {
        id: 4,
        title: 'Some note 4',
        content: 'Do anything',
        done: false
      },
      {
        id: 5,
        title: 'Some note 5',
        content: 'Do anything',
        done: false
      },
      {
        id: 6,
        title: 'Some note 6',
        content: 'Do anything',
        done: false,
        archive: true
      }
    ]
    return notes
  }
}
