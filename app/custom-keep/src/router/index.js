import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import ArchivePage from '@/components/ArchivePage'
import UpdateNote from '@/components/UpdateNote'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueResource from 'vue-resource'
import VueResourceMocker from 'vue-resource-mocker'
import MockData from '../mock/mock'

Vue.use(VueResource)
Vue.httpMocker = new VueResourceMocker()
Vue.use(Vue.httpMocker)

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/main',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/archive',
      name: 'ArchivePage',
      component: ArchivePage
    },
    {
      path: '/note/:id',
      name: 'UpdateNote',
      component: UpdateNote
    }
  ]
})

Vue.httpMocker.setRoutes({
  GET: MockData
})
