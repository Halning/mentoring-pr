import { Observable } from 'rxjs/internal/Observable';

export const filterByString = (str: string) => <T>(source: Observable<T>) =>
    new Observable<T>((observer) => {
        return source.subscribe({
            next(x) {
                if (typeof x === 'string' && str.includes(x)) {
                    observer.next(x);
                }
            },
            error(err) {
                observer.error(err);
            },
            complete() {
                observer.complete();
            },
        });
    });
