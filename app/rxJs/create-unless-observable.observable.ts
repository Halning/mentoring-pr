import { Observable } from 'rxjs/internal/Observable';
import { interval } from 'rxjs/internal/observable/interval';
import { map } from 'rxjs/operators';
import { filterByString } from './filter-by-string.operator';
import { switchCase } from './switch-case.operator';

export function createUselessObs(): Observable<string> {
    return interval(1000)
        .pipe(
            map(() => getRandomSymbol()),
            filterByString('absdiefgABCDIEFG'),
            switchCase(),
        );
}

function getRandomSymbol(): string {
    const randomSymbol = Math.random().toString(36).substr(2, 1);
    return getRandomLowerOrUpperCase(randomSymbol);
}

function getRandomLowerOrUpperCase(symbol: string): string {
    const randomNumber = Math.floor(Math.random() * (2));

    return randomNumber
        ? symbol.toUpperCase()
        : symbol.toLowerCase();
}
