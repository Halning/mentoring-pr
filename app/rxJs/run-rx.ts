import { createUselessObs } from './create-unless-observable.observable';
import { CustomSkipSubject } from './custom-skip.subject';

export function runRxJs() {
    createUselessObs()
        .subscribe((data) => console.log(data));

    const mySubject = new CustomSkipSubject(2);
    mySubject.subscribe((data) => console.log('Sub1', data));
    mySubject.next(1);
    mySubject.next(2);
    mySubject.next(3);
    mySubject.next(4);
    mySubject.subscribe((data) => console.log('Sub2', data));
    mySubject.next(5);
    mySubject.next(6);
    mySubject.next(7);
    mySubject.next(8);
}
