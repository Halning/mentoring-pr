/** Observer pattern */

// subscribe(subscriber: Subscriber<T>): Subscription {
//     if (this.closed) {
//         throw new ObjectUnsubscribedError();
//     } else if (this.hasError) {
//         subscriber.error(this.thrownError);
//         return Subscription.EMPTY;
//     } else if (this.isStopped) {
//         subscriber.complete();
//         return Subscription.EMPTY;
//     } else {
//         this.observers.push(subscriber);
//         return new SubjectSubscription(this, subscriber);
//     }
// }

/** Modules pattern */

// export class Observable<T> implements Subscribable<T> {

// import { Observable } from './Observable';

/** Iterator pattern */

// next(value?: T) {
//     if (this.closed) {
//         throw new ObjectUnsubscribedError();
//     }
//     if (!this.isStopped) {
//         const { observers } = this;
//         const len = observers.length;
//         const copy = observers.slice();
//         for (let i = 0; i < len; i++) {
//             copy[i].next(value);
//         }
//     }
// }

/** Proxy pattern */

// call(subscriber: Subscriber<T>, source: any): TeardownLogic {
//     return source.subscribe(new FilterSubscriber(subscriber, this.predicate, this.thisArg));
// }

/** constructor */

// constructor(destination: Subscriber<T>,
//     private predicate: (value: T, index: number) => boolean,
//     private thisArg: any) {
//     super(destination);
// }
