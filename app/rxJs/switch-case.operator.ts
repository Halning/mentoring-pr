import { Observable } from 'rxjs/internal/Observable';

export const switchCase = () => <T>(source: Observable<T>) =>
    new Observable<string>((observer) => {
        return source.subscribe({
            next(x) {
                if (typeof x === 'string') {
                    observer.next(
                        isCamelCase(x)
                            ? x.toLowerCase()
                            : x.toUpperCase(),
                    );
                }
            },
            error(err) {
                observer.error(err);
            },
            complete() {
                observer.complete();
            },
        });
    });

function isCamelCase(str: string): boolean {
    return /[A-Z]/.test(str);
}
