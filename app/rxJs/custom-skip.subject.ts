import { Subject } from 'rxjs/internal/Subject';
import { Observer } from 'rxjs/internal/types';
import { ObjectUnsubscribedError } from 'rxjs/src/internal/util/ObjectUnsubscribedError';

export class CustomSkipSubject<T> extends Subject<T> {
    constructor(private skipCount: number = 0) {
        super();
    }

    next(value?: T) {
        if (this.closed) {
            throw new ObjectUnsubscribedError();
        }
        if (!this.isStopped) {
            const {observers, skipCount} = this;

            const len = observers.length;
            const copy = observers.slice();
            for (let i = 0; i < len; i++) {
                if (!copy[i].hasOwnProperty('emitCounter')) {
                    this.defineEmitCounterToSubscription(copy[i]);
                } else {
                    copy[i]['emitCounter']++;
                }
            }

            for (let i = 0; i < len; i++) {
                if (copy[i]['emitCounter'] >= skipCount) {
                    copy[i].next(value);
                }
            }
        }
    }

    private defineEmitCounterToSubscription(sub: Observer<T>): void {
        Object.defineProperty(
            sub,
            'emitCounter',
            {
                configurable: false,
                enumerable: false,
                value: 0,
                writable: true,
            },
        );
    }
}