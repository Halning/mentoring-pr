import { runFunPr } from './functional-pr/run';
import { runRxJs } from './rxJs/run-rx';

document.addEventListener('DOMContentLoaded', ready);

/**
 * show developer name
 */
export function ready(): void {
    const subtitle = document.querySelector('.bem__vitalik');
    subtitle.textContent = 'Andrii Khomenko1';

    runRxJs();

    runFunPr();
}
