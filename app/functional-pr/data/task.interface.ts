export type PickTask = Pick<ITask, 'name'>;

export interface ITask {
    name: string;
    duration: number;
    type: 'work' | 'common' | 'private';
}
