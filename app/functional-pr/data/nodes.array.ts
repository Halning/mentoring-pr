export const nodes = [
    {
        id: '1',
        type: 'div',
        className: 'black red',
        children: [
            {
                id: '2',
                type: 'div',
                className: 'red',
                children: [
                    {
                        id: '3',
                        type: 'text',
                        className: 'green',
                        children: null,
                    },
                ],
            },
        ],
    },
    {
        id: '4',
        type: 'div',
        className: 'green white',
        children: [{
            id: '5',
            type: 'div',
            className: 'red white',
            children: [
                {
                    id: '6',
                    type: 'span',
                    className: 'red',
                    children: [
                        {
                            id: '8',
                            type: 'text',
                            className: 'green',
                            children: null,
                        },
                    ],
                },
                {
                    id: '7',
                    type: 'text',
                    className: 'red green',
                    children: null,
                },
            ],
        }],
    },
];
