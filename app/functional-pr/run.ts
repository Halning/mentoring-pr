import { tasks } from './data/tasks.array';
import { mapTaskDuration } from './functional-programming.task2';
import { takeTaskDuration } from './functional-programming.task1';
import { accDuration, map, mapTaskName, reduce } from './functional-programming.task3';
import { ITask } from './data/task.interface';
import { nodes } from './data/nodes.array';
import { mapNodes } from './functional-programming.task4';

export function runFunPr() {
    console.log(`Func programing task1`, takeTaskDuration(tasks));
    console.log(`Func programing task2`, mapTaskDuration(tasks));
    console.log(`Func programing task2`, mapTaskDuration(tasks));
    console.log(`Func programing task3`, reduce<ITask, number>(tasks, accDuration, 0));
    console.log(`Func programing task3`, map<ITask, string[]>(tasks, mapTaskName));
    console.log(`Func programing task4`, mapNodes(nodes));
}
