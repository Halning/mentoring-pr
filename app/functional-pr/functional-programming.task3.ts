export const accDuration = (acc, item) => {
    return acc + item.duration;
};

export const mapTaskName = (item) => {
    return item.name;
};

export function reduce<T, U>(items: T[], callbackFn: any, acc: U): U {
    const itemsCopy = items.slice();

    itemsCopy.forEach((item) => {
        acc = callbackFn(acc, item);
    });

    return acc;
}

export function map<T, U>(items: T[], callbackFn: any): U[] {
    const itemsCopy = items.slice();
    const result = [];

    itemsCopy.forEach((item) => {
        result.push(callbackFn(item));
    });

    return result;
}
