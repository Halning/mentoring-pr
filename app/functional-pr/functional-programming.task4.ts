export function mapNodes(inputNodes: any[], nodeParent = null, acc = [], result = [], inc = 0): any[] {
    const node = inputNodes[inc++];

    if (node && node.children && node.children.length) {
        mapNodes(node.children, node, acc, result);
    }

    if (node) {
        mapNodes(inputNodes, nodeParent, acc, result, inc);
        filterByChildColor(nodeParent, acc, node, result);
    }
    return result;
}

function filterByChildColor(nodeParent: any, acc: string[], node: any, result: any[]): void {
    if (nodeParent && acc.indexOf(nodeParent.id) === -1 && (node.className.includes('green') || acc.indexOf(node.id) !== -1)) {
        acc.push(nodeParent.id);
        result.push({...nodeParent, children: null});
    }
}
