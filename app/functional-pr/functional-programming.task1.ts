import { ITask, PickTask } from './data/task.interface';

export function takeTaskDuration(mapTasks: ITask[]): PickTask {
    const filteredTasks = filterTaskByType<ITask>(mapTasks);

    return filteredTasks.reduceRight((acc: PickTask, task: ITask) => {
        acc[task.name] = addValuesByTaskName(acc, task);
        return acc;
    }, {} as PickTask);
}

function filterTaskByType<T>(items: T[]): T[] {
    return items.filter((item: any) => {
        return item.type === 'private' || item.type === 'work';
    });
}

function addValuesByTaskName(acc: any, task: ITask): number {
    return acc && acc[task.name]
        ? acc[task.name] + task.duration
        : task.duration;
}
