import * as R from 'ramda';
import { ITask, PickTask } from './data/task.interface';

export const mapTaskDuration = R.reduce((acc: PickTask, task: ITask): PickTask => {
    const accMap = R.ifElse(
        R.either(() => task.type === 'work', () => task.type === 'private'),
        () => {
            acc[task.name] = acc && acc[task.name]
                ? acc[task.name] + task.duration
                : task.duration;
            return acc;
        },
        () => acc,
    );
    return accMap(acc);
}, {});
