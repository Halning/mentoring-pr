import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as mongoose from 'mongoose';
import { MachineApiRoutes } from './routes/machine-api-routes';

export class MachineApi {

    public app: express.Application;
    public routePrv: MachineApiRoutes = new MachineApiRoutes();

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.config();
        this.routePrv.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
    }

    private mongoSetup(): void {
        mongoose.Promise = global.Promise;
    }

}
