import { Request, Response } from 'express';
import * as mongoose from 'mongoose';
import { GroupDTO } from '../models/device/group-dto.model';
import { GroupSchema } from '../models/device/group.schema';
import { Device } from './control-device.controller';

export const Group = mongoose.model('Group', GroupSchema);

export class GroupDeviceController {

    public async addNewGroup(req: Request, res: Response) {
        const newGroup = new Group(req.body);

        try {
            const group = await newGroup.save();
            res.json(group);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async deleteGroup(req: Request, res: Response) {
        try {
            await Group.remove({_id: req.params.groupId});
            res.json({message: 'Successfully deleted group!'});
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async getGroups(req: Request, res: Response) {
        try {
            const group = await Group.find({});
            res.json(group);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async getGroupWithID(req: Request, res: Response) {
        try {
            const group = await Group.findById(req.params.groupId);
            res.json(group);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async updateGroupState(req: Request, res: Response) {
        try {
            const oldGroup = await Group.findById(req.params.groupId);
            const groupDevice = new GroupDTO(req.body);
            if (oldGroup.devicesState !== groupDevice.devicesState) {
                await Device.updateMany(
                    {_id: {$in: oldGroup.devicesIds}},
                    {state: groupDevice.devicesState},
                    {multi: true},
                );
            }

            await Group.findOneAndUpdate(
                {_id: req.params.groupId},
                new GroupDTO(req.body),
                {new: true},
            );

            res.json(groupDevice);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    private errorHandler(res: Response, err: any) {
        console.log('My Error', err);
        res.status(err.statusCode || 500).send(err);
    }
}
