import { Request } from 'express';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import * as request from 'request-promise';
import { DeviceDTO } from '../models/device/device-dto.model';
import { DeviceSchema } from '../models/device/device.schema';
import { LogDeviceSchema } from '../models/device/log-device.schema';

export const Device = mongoose.model('Device', DeviceSchema);
export const Log = mongoose.model('Log', LogDeviceSchema);

export class ControlDeviceController {

    public async addNewDevice(req: Request, res: Response) {
        const newDevice = new Device(req.body);

        try {
            const device = await newDevice.save();
            res.json(device);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async getDevices(req: Request, res: Response) {
        try {
            const device = await Device.find({});
            res.json(device);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async getDeviceWithID(req: Request, res: Response) {
        try {
            const device = await Device.findById(req.params.deviceId);
            res.json(device);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async updateDeviceState(req: Request, res: Response) {
        try {
            const oldDevice = await Device.findById(req.params.deviceId);
            const device = new DeviceDTO(req.body);
            if (device.address && device.state !== oldDevice.state) {
                await this.patchMachineRequest(device);
            }

            await Device.findOneAndUpdate(
                {_id: req.params.deviceId},
                device,
                {new: true},
            );

            if (device.state !== oldDevice.state) {
                await this.logUpdateDeviceState(
                    {
                        changed: device.state,
                        deviceId: req.params.deviceId,
                    });
            }

            res.send({
                message: [`Machine state changed to ${device.state}`, 'Device changed'],
            });

        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async deleteDevice(req: Request, res: Response) {
        try {
            await Device.remove({_id: req.params.deviceId});
            res.json({message: 'Successfully deleted device!'});
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    public async getDeviceLogState(req: Request, res: Response) {
        try {
            const logs = await Log.find({deviceId: req.params.deviceId});
            res.json(logs);
        } catch (err) {
            this.errorHandler(res, err);
        }
    }

    private logUpdateDeviceState(device): Promise<{} | void> {
        const newLog = new Log(device);

        return newLog.save();
    }

    private async patchMachineRequest(device: DeviceDTO): Promise<any> {
        if (device.state === 'on') {
            return await request({
                method: 'patch',
                url: `${device.address}/api/machine/on`,
            });
        } else if (device.state === 'off') {
            return await request({
                method: 'patch',
                url: `${device.address}/api/machine/off`,
            });
        }
    }

    private errorHandler(res: Response, err: any) {
        console.log('My Error', err);
        res.status(err.statusCode || 500).send(err);
    }
}
