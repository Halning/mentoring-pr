export class MachineApiController {

    machineState: 'on' | 'off';
    set setMachineState(value: 'on' | 'off') {
        this.machineState = value;
    }
}
