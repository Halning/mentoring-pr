import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const LogDeviceSchema = new Schema({
    deviceId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Device',
        trim: true
    },
    changeDate: {
        type: Date,
        default: Date.now
    },
    changed: {
        type: String
    }
});
