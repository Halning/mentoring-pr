import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const DeviceSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: 'Enter a first name',
    },
    address: {
        type: String,
    },
    state: {
        type: String,
    },
    groupId: {
        type: Number,
    },
});
