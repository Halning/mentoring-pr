import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const GroupSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: 'Enter a group name',
    },
    devicesState: {
        type: String,
    },
    devicesIds: {
        type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Device'}],
    },
});
