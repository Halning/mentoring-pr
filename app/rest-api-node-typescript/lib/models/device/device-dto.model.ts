export class DeviceDTO {
    constructor(options: Partial<DeviceDTO>,
                public name: string = options.name,
                public address: string = options.address,
                public state: 'on' | 'off' = options.state,
                public groupId: number = options.groupId) {
    }
}