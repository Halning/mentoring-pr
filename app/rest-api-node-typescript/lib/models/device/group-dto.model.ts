export class GroupDTO {
    constructor(options: Partial<GroupDTO>,
                public name: string = options.name,
                public devicesState: 'on' | 'off' = options.devicesState,
                public devicesIds: number[] = options.devicesIds) {
    }
}