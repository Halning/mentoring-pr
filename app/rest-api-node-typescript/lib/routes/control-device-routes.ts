import { Response } from 'express';
import { Request } from 'express';
import * as express from 'express';
import { ControlDeviceController } from '../controllers/control-device.controller';

const router = express.Router();

export class ControlDeviceRoutes {

    public deviceController: ControlDeviceController = new ControlDeviceController();

    public routes(app): void {

        // Devices
        router
            .get('', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.deviceController.getDevices(req, res);
            });

        // add Device
        router
            .post('', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.deviceController.addNewDevice(req, res);
            });

        // get, delete, update device
        router
            // .get('/:deviceId', this.deviceController.getDeviceWithID)
            .patch('/:deviceId', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.deviceController.updateDeviceState(req, res);
            })
            .delete('/:deviceId', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.deviceController.deleteDevice(req, res);
            });

        router
            .get('/:deviceId/logs', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.deviceController.getDeviceLogState(req, res);
            });

        app.use('/api/control-device/devices', router);
    }
}
