import { Request } from 'express';
import { Response } from 'express';
import * as express from 'express';
import { GroupDeviceController } from '../controllers/group-device.controller';

const router = express.Router();

export class GroupDeviceRoutes {

    public groupDeviceController: GroupDeviceController = new GroupDeviceController();

    public routes(app): void {

        router
            .get('', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.groupDeviceController.getGroups(req, res);
            })
            .post('', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.groupDeviceController.addNewGroup(req, res);
            });

        // get, delete, update device
        router
            .get('/:groupId', this.groupDeviceController.getGroupWithID)
            .patch('/:groupId', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.groupDeviceController.updateGroupState(req, res);
            })
            .delete('/:groupId', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.groupDeviceController.deleteGroup(req, res);
            });

        app.use('/api/control-device/groups', router);
    }
}
