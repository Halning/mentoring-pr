import { Request } from 'express';
import { Response } from 'express';
import * as express from 'express';
import { MachineApiController } from '../controllers/machine-api.controller';

const router = express.Router();

export class MachineApiRoutes {

    public machineController: MachineApiController = new MachineApiController();

    public routes(app): void {

        // Machine
        router
            .patch('/on', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.machineController.setMachineState = 'on';
                res.status(200).send({
                    message: `Machine state changed to on`,
                });
            });

        router
            .patch('/off', (req: Request, res: Response) => {
                console.log(`Request from: ${req.originalUrl}`);
                console.log(`Request type: ${req.method}`);
                this.machineController.setMachineState = 'off';
                res.status(200).send({
                    message: `Machine state changed to on`,
                });
            });

        app.use('/api/machine', router);
    }
}
