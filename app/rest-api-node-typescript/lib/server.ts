import * as http from 'http';
import { DeviceControlApp } from './device-control-app';
import { MachineApi } from './machine-api';

const enum App {
    CONTROL_DEVICE_PORT = 3000,
    DEVICE_PORT = 3002,
}

http.createServer(new DeviceControlApp().app).listen(App.CONTROL_DEVICE_PORT, () => {
    console.log('Express server listening on port ' + App.CONTROL_DEVICE_PORT);
});

http.createServer(new MachineApi().app).listen(App.DEVICE_PORT, () => {
    console.log('Express server listening on port ' + App.DEVICE_PORT);
});
