import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as mongoose from 'mongoose';
import { ControlDeviceRoutes } from './routes/control-device-routes';
import { GroupDeviceRoutes } from './routes/group-device-routes';

export class DeviceControlApp {

    public app: express.Application;
    public routeControlDevice: ControlDeviceRoutes = new ControlDeviceRoutes();
    public routeGroupDevice: GroupDeviceRoutes = new GroupDeviceRoutes();
    public mongoUrl = 'mongodb://localhost/CRMdb';

    // public mongoUrl: string = 'mongodb://dalenguyen:123123@localhost:27017/CRMdb';

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.config();
        this.routeControlDevice.routes(this.app);
        this.routeGroupDevice.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
        // serving static files
        this.app.use(express.static('public'));
    }

    private mongoSetup(): void {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl);
    }

}
