import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Device } from '../models/device.model';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css'],
})
export class DeviceListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'name', 'state', 'address'];
  dataSource!: MatTableDataSource<Device>;
  selection = new SelectionModel<Device>(true, []);

  devicesData!: Device[];

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get('/api/control-device/devices')
      .subscribe((res: any) => {
        console.log(res);
        this.devicesData = res.map((device: {}) => new Device(device));
        this.dataSource = new MatTableDataSource<Device>(this.devicesData);
        console.log(this.devicesData);
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  addColumn() {
    console.log(this.selection);
    // const randomColumn = Math.floor(Math.random() * this.displayedColumns.length);
    // this.columnsToDisplay.push(this.displayedColumns[randomColumn]);
  }

  removeColumn() {
    // if (this.columnsToDisplay.length) {
    //   this.columnsToDisplay.pop();
    // }
  }
}
