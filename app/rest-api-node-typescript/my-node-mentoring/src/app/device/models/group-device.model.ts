export class GroupDevice {
  constructor(options: Partial<NonNullable<GroupDevice>> = {},
              public _id: string | null = options._id || null,
              public name: string = options.name || 'new group',
              public devicesState: 'on' | 'off' = options.devicesState || 'off',
              public devicesIds: string[] | null = options.devicesIds || null) {
  }
}
