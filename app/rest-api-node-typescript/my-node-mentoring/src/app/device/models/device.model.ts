export class Device {
  constructor(options: Partial<NonNullable<Device>> = {},
              public _id: string | null = options._id || null,
              public name: string = options.name || 'sd',
              public address: string = options.address || 'http://localhost:3002',
              public state: 'on' | 'off' = options.state || 'off',
              public groupId: number | null = options.groupId || null) {
  }
}
