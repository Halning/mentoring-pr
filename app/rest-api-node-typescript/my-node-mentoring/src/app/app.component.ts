import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Device } from './device/models/device.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.patch(
      `/api/control-device/devices/${'5b9fcf058816761534b43e09'}`,
      new Device({state: 'off', groupId: 10}),
    )
      .subscribe((res) => {
        console.log(res);
      });

    this.http.post(
      `/api/control-device/devices`,
      new Device(),
    )
      .subscribe((res) => {
        console.log(res);
      });

    this.http.get(
      `/api/control-device/devices/${'5b9fcf058816761534b43e09'}/logs`,
    )
      .subscribe((res) => {
        console.log(res);
      });
  }

}
