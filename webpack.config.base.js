const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: [
        './app/app.ts',
        './app/styles/style.scss',
        './app/styles/style.less',
        './app/styles/style.styl',
        './app/cof.coffee',
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                'commons': {
                    minChunks: 2,
                    chunks: 'all',
                    name: 'commons',
                    priority: 10,
                    enforce: true,
                },
            },
        },
    },
    module: {
        rules: [
            {
                test: /\.coffee$/,
                use: ['coffee-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Andrii template',
            template: './app/index.html',
        }),
        new CleanWebpackPlugin('dist'),
    ],
};
